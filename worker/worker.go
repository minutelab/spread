package worker

import (
	"fmt"
	"os/exec"
	"syscall"

	"bitbucket.org/minutelab/spread/env"
	"bitbucket.org/minutelab/spread/job"
	"bitbucket.org/minutelab/spread/serialout"
)

// Start start a worker that run all the jobs with the specified environemnt
// When the job queue closes it return in retChan its accumulated output
func Start(name string, e env.Map, out serialout.Serializer, queue <-chan job.Job, retChan chan<- int) {
	out = serialout.NewDerived(name, out)
	var rc RC
	defer func() {
		out.Debug("ending rc=%d", rc.Value())
		retChan <- rc.Value()
	}()

	out.Debug("started")

	for job := range queue {
		out.Debug("running %s", job.Name())

		outFile, err := job.Run(e.List())

		r, rErr := extractRC(err)
		rc.Append(r)

		if outFile == nil {
			out.Debug("failed running: %s", err)
			continue
		}

		title := job.Name()
		switch {
		case rErr != nil:
			title = fmt.Sprintf("%s - Error: %s", title, rErr)
		case rc > 0:
			title = fmt.Sprintf("%s - rc: %d", title, r)
		}
		out.WriteStream(title, outFile)
	}

}

// getRC get a numeric return code from an error
// If it is an error but it fails to get a numeric code it will return 1
func extractRC(err error) (int, error) {
	if err == nil {
		return 0, nil
	}
	if err, ok := err.(*exec.ExitError); ok {
		if status, ok := err.Sys().(syscall.WaitStatus); ok {
			return status.ExitStatus(), nil
		}
	}
	return 1, err
}
