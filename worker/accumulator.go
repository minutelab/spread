package worker

// RC "accumulates" return codes
// The main property is that if all it accumulate are 0 (success) the end result is successfully
// And if one is non-zero the result is non zero
//
// The actual implementation is to take the maxium absloute value
type RC int

// Append rc to the accumulation
func (r *RC) Append(rc int) {
	if rc < 0 {
		rc = -rc
	}
	if rc > r.Value() {
		*r = RC(rc)
	}
}

// Value return the current value
func (r *RC) Value() int { return int(*r) }
