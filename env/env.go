// Package env handle environment variables in a predictable way
//
// it is going to be somewhat obsolete with go 1.9
package env

import (
	"os"
	"strings"
)

// Map store environment as a map key ->  value
type Map struct {
	m map[string]string
}

// New create an environment map from a list of key=value strigns
// (the same kind of list that os.Environ returns)
func New(list []string) Map {
	m := make(map[string]string)
	for _, e := range os.Environ() {
		vals := strings.SplitN(e, "=", 2)
		if len(vals) < 2 {
			continue
		}
		m[vals[0]] = vals[1]
	}

	return Map{m: m}
}

// Set the value of environment variable
func (m *Map) Set(key, val string) {
	m.m[key] = val
}

// List return the environment as list of key=value strigns (suitable for exec.Command)
func (m *Map) List() []string {
	l := make([]string, 0, len(m.m))
	for k, v := range m.m {
		l = append(l, k+"="+v)
	}
	return l
}
