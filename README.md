# Spread - spread job among multiple MinuteLab hosts

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

A simplistic way to spread a list of jobs among several Minute Lab hosts

## Introduction

The general model is to create a jobfile. Each line in the job is a shell command that
can be performed independently.
It is expected that this command is an mlab script, or something that call an mlab script.
(but it is not required).

Spread will call those jobs in parallel assigning,
making sure that each host would run a fixed number of jobs in parallel.

So if you have n hosts, and you can run m jobs on each hosts meaning that you will have m times n jobs
running in parallel.

spread will run all jobs, and it return code will be zero (success) if and only if all jobs succeeded.

Despite the fact that the jobs will run in parallel spread will serialize the output:
The output of each job is buffered until the job ends, and then it is printed as a whole.
so output of several jobs is not interleaved.

## Usage

The usage is simple:

```
spread [-d/--debug] [-w/--workers <workers>] [-H/--Hosts <list>] <jobfile>
```

Where the job file contain a line per job. Lines containing only white spaces are ignored,
as well as lines whose first non-white space is '#' (comments)

`-w` determine how many jobs will run in parallel on each host.

`-H` provide a comma separate list of mlab hosts to run on. `-H-` will cause spread
to use all available hosts. If `-H` is not specified spread would not attempt to spread to different hosts.


### Environment for each job

When each job is run, two variables are added to the environment:

* `WORKER_ID` - give the ID of the worker (numerical value starting at 1)
* `MLAB_SERVER` - the mlab server to run the script on. mlab scripts uses this automatically to determine the host
they will run on.
