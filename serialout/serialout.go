// Package serialout serialize text output from several goroutines so it won't mix
package serialout

import (
	"context"
	"errors"
	"fmt"
	"io"
	"time"
)

// Serializer is the basic interface for serialization
// It can serialize strings and streams (represented as io.ReadCloser)
type Serializer interface {
	Debug(f string, a ...interface{}) error
	WriteMsg(f string, a ...interface{}) error
	WriteStream(title string, r io.ReadCloser) error
	Close() error
}

// serializer is the base serializtion interface
type serializer struct {
	w      io.Writer
	inp    chan<- outputer
	ctx    context.Context
	cancel func()
	wait   chan interface{}
	debug  bool
}

// outputer is an interface for an object that is able
// to output it self to io.Writer
type outputer interface {
	output(io.Writer) error
}

// NewSerializer create a new serializer that write to the specified io.Writer.
// Closing this serializer will wait for all output to be written
func NewSerializer(w io.Writer, debug bool) Serializer {
	ctx, cancel := context.WithCancel(context.Background())
	c := make(chan outputer)

	s := &serializer{
		w:      w,
		inp:    c,
		ctx:    ctx,
		cancel: cancel,
		wait:   make(chan interface{}),
		debug:  debug,
	}
	go s.loop(c)
	return s
}

func (s *serializer) Close() error {
	s.cancel()
	<-s.wait
	return nil
}

func (s *serializer) output(o outputer) error {
	select {
	case s.inp <- o:
		return nil
	case <-s.ctx.Done():
		return errors.New("closed")
	}
}

func (s *serializer) loop(c <-chan outputer) {
	defer s.cancel()
	defer close(s.wait)
	for {
		select {
		case o := <-c:
			if o == nil {
				fmt.Fprintln(s.w, "Error: nil outputer")
				break
			}
			if err := o.output(s.w); err != nil {
				fmt.Fprintln(s.w, "Error writing: ", err)
			}
		case <-s.ctx.Done():
			return
		}
	}
}

func (s *serializer) WriteMsg(f string, a ...interface{}) error {
	return s.output(message(fmt.Sprintf(f, a...)))
}

func (s *serializer) Debug(f string, a ...interface{}) error {
	if s.debug {
		return s.WriteMsg(timeStamp(f), a...)
	}
	return nil
}

func timeStamp(m string) string {
	return "[" + time.Now().Format("15:04:05") + "] " + m
}

func (s *serializer) WriteStream(title string, r io.ReadCloser) error {
	o := readerOutputer{r: r}
	if s.debug {
		o.title = title
	}
	return s.output(o)
}

type message string

func (m message) output(w io.Writer) error {
	_, err := fmt.Fprintln(w, m)
	return err
}

type readerOutputer struct {
	title string
	r     io.ReadCloser
}

func (r readerOutputer) output(w io.Writer) error {
	if r.title != "" {
		fmt.Fprintln(w, "<<<< ", r.title)
	}

	lw := &lastByte{w: w}
	_, err := io.Copy(lw, r.r)
	if lw.last != '\n' {
		_, err2 := w.Write([]byte{'\n'})
		err = accumulateErr(err, err2)
	}

	if r.title != "" {
		fmt.Fprintln(w, ">>>> ", r.title)
	}

	return accumulateErr(err, r.r.Close())
}

func accumulateErr(err1, err2 error) error {
	if err1 != nil {
		return err1
	}
	return err2
}

// last byte wraps a writer and allow us to get the last byte written
type lastByte struct {
	w    io.Writer
	last byte
}

func (l *lastByte) Write(p []byte) (int, error) {
	n, err := l.w.Write(p)
	if n > 0 {
		l.last = p[n-1]
	}
	return n, err
}

type derivedSerializer struct {
	title string
	base  Serializer
}

// NewDerived create a derived Serializer that uses the base to output message,
// but prepend the given title to all messages
// the title is Stringer, so it can include time stamp (computed at the time of writing)
//
// Closing the derived serializer is a non-operation (it doesn't close the base or do anything)
func NewDerived(title string, base Serializer) Serializer {
	return &derivedSerializer{title: title, base: base}
}

func (d *derivedSerializer) WriteMsg(f string, a ...interface{}) error {
	return d.base.WriteMsg("%s %s", d.title, fmt.Sprintf(f, a...))
}

func (d *derivedSerializer) Debug(f string, a ...interface{}) error {
	return d.base.Debug("%s %s", d.title, fmt.Sprintf(f, a...))
}

func (d *derivedSerializer) WriteStream(title string, r io.ReadCloser) error {
	return d.base.WriteStream(d.title+" "+title, r)
}

func (d *derivedSerializer) Close() error { return nil }
