package main

import (
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"

	"github.com/alecthomas/kingpin"

	"bitbucket.org/minutelab/spread/env"
	"bitbucket.org/minutelab/spread/job"
	"bitbucket.org/minutelab/spread/serialout"
	"bitbucket.org/minutelab/spread/worker"
)

var cmdCtx struct {
	jobsFile *os.File
	debug    bool
	workers  int
	hosts    string
}

func main() {
	app := kingpin.New("spread", "spread jobs across multiple workers")
	app.Arg("jobs", "file containing jobs description").Required().FileVar(&cmdCtx.jobsFile)
	app.Flag("workers", "number of workers (per host)").Short('w').Default("2").IntVar(&cmdCtx.workers)
	app.Flag("debug", "debug output").Short('d').BoolVar(&cmdCtx.debug)
	app.Flag("hosts", "mlab hosts (comma separated, '-',means auto)").Short('H').StringVar(&cmdCtx.hosts)
	app.Action(doMain)
	if _, err := app.Parse(os.Args[1:]); err != nil {
		app.Fatalf("%s", err)
	}
}

func doMain(_ *kingpin.ParseContext) error {
	jobs, err := job.Read(cmdCtx.jobsFile)
	if err != nil {
		return fmt.Errorf("reading jobs: %s", err)
	}

	hosts, err := getHosts()
	if err != nil {
		return fmt.Errorf("getting hosts: %s", err)
	}

	out := serialout.NewSerializer(os.Stdout, cmdCtx.debug)
	out.Debug("%d jobs using %d workers", len(jobs), cmdCtx.workers)

	queue := make(chan job.Job)
	retChan := make(chan int)

	id := 0
	for _, h := range hosts {
		// Start workers
		for i := 0; i < cmdCtx.workers; i++ {
			id++
			e := env.New(os.Environ())
			e.Set("WORKER_ID", strconv.Itoa(id))
			if h != "" {
				e.Set("MLAB_SERVER", h)
			}
			go worker.Start(fmt.Sprintf("Worker %d", id), e, out, queue, retChan)
		}
	}

	// Queue jobs
	for _, j := range jobs {
		queue <- j
	}
	close(queue)

	// Collect return codes
	var rc worker.RC
	for i := 0; i < cmdCtx.workers*len(hosts); i++ {
		rc.Append(<-retChan)
	}

	out.Debug("Ending: return code: %d", rc.Value())
	out.Close()

	if rc > 0 {
		os.Exit(rc.Value())
	}
	return nil
}

func getHosts() ([]string, error) {
	if cmdCtx.hosts == "" {
		return []string{""}, nil
	}

	if cmdCtx.hosts == "-" {
		return getAutoHosts()
	}

	return strings.Split(cmdCtx.hosts, ","), nil
}

func getAutoHosts() ([]string, error) {
	cmd := exec.Command("mlab", "config", "servers", "list", "-j")
	out, err := cmd.CombinedOutput()
	if err != nil {
		return nil, fmt.Errorf("Error: %s - %s", err, string(out))
	}

	hostMap := make(map[string]interface{})
	if err := json.Unmarshal(out, &hostMap); err != nil {
		return nil, err
	}

	var list []string
	for h := range hostMap {
		list = append(list, h)
	}
	return list, nil
}
