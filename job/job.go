package job

import (
	"bufio"
	"io"
	"io/ioutil"
	"os/exec"
	"strings"
)

// Job represent a job to be done
type Job struct {
	cmd string
}

// Name return a Name for the job
func (j *Job) Name() string { return j.cmd }

// Run the jobs, and return its output (in a temp file)
func (j *Job) Run(env []string) (io.ReadCloser, error) {
	tmpFile, err := ioutil.TempFile("", "spread.")
	if err != nil {
		tmpFile.Close()
		return nil, err
	}

	cmd := exec.Command("sh", "-c", j.cmd)
	cmd.Stdout = tmpFile
	cmd.Stderr = tmpFile
	cmd.Env = env

	err = cmd.Run()
	tmpFile.Seek(0, io.SeekStart)
	return tmpFile, err
}

// Read job descriptiohn from a file
func Read(r io.Reader) ([]Job, error) {
	var jobs []Job
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		line := strings.TrimSpace(scanner.Text())
		if len(line) == 0 || line[0] == '#' {
			continue
		}
		jobs = append(jobs, Job{cmd: line})
	}
	if err := scanner.Err(); err != nil {
		return nil, err
	}
	return jobs, nil
}
